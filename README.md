![picture](https://i.imgur.com/Zq7Cymc.png)

Unofficial Ruffle repository that compiles binaries for Linux x64,arm64 and i386

Note: while this builds against the nightly builds this repo won't be doing daily builds and will be going at a slower pace.

&nbsp;&nbsp;&nbsp;&nbsp;

  You can install Ruffle-bin from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/ruffle-bin/)
  
 [Click to get the latest release](https://gitlab.com/ruffle-linux/ruffle/-/releases)

 ### Author
  * Corey Bruce

